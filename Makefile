dev:
	docker run -ti -v $${PWD}:/app -w /app golang:1.19-alpine

build:
	docker build -t mygin .

run:
	docker run -ti -p 8080:8080 mygin
