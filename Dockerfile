FROM golang:1.19.4-alpine3.17 as build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN go build -o /app/server

FROM golang:1.19.4-alpine3.17
RUN addgroup -S notrootgroup && adduser -S notrootuser -G notrootgroup
USER notrootuser
WORKDIR /app
COPY --from=build --chown=notrootuser:notrootgroup /app/server ./
ENV GIN_MODE=release
CMD [ "/server" ]
